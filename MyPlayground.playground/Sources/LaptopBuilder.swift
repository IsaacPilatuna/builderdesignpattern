import Foundation
public class LaptopBuilder:Builder{

    public init(){}

    var laptop:Laptop=Laptop()
    
    public func reset(){
        laptop=Laptop()
    }
    public func setCPU(type:String){
        laptop.cpu=type
    }
    public func setRAM(size:Int){
        laptop.ram=size
    }
    public func setDiskType(type:String){
        laptop.diskType=type
    }
    public func setDiskSize(size:Int){
        laptop.diskSize=size
    }
    public func setVideoCardType(type:String){
        laptop.videoCardType=type
    }
    public func getResult()->Laptop{
        return laptop
    }
}
