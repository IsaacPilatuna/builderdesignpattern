import Foundation

public protocol Builder{
    func reset()
    func setCPU(type:String)
    func setRAM(size:Int)
    func setDiskType(type:String)
    func setDiskSize(size:Int)
    func setVideoCardType(type:String)
}
