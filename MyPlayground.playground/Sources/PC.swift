import Foundation
public class PC{
    public init(){}
    public var cpu:String = ""
    public var ram:Int = 0
    public var diskType:String = ""
    public var diskSize:Int = 0
    public var videoCardType:String = ""
    public var description: String { return "PC{ CPU:\(cpu), RAM:\(ram), diskType:\(diskType), diskSize:\(diskSize), videoCardType:\(videoCardType)}" }
}
