import Foundation
public class PCBuilder:Builder{

    public init(){}

    var pc:PC=PC()
    
    public func reset(){
        pc=PC()
    }
    public func setCPU(type:String){
        pc.cpu=type
    }
    public func setRAM(size:Int){
        pc.ram=size
    }
    public func setDiskType(type:String){
        pc.diskType=type
    }
    public func setDiskSize(size:Int){
        pc.diskSize=size
    }
    public func setVideoCardType(type:String){
        pc.videoCardType=type
    }
    public func getResult()->PC{
        return pc
    }
}
