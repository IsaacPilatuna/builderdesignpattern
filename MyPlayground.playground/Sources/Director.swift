import Foundation
public class Director{


    public init(){}

    public func makeRegularLaptop(builder:LaptopBuilder)->LaptopBuilder{
        builder.reset()
        builder.setCPU(type:"i5-5100U")
        builder.setRAM(size:4)
        builder.setDiskType(type:"HDD")
        builder.setDiskSize(size:512)
        builder.setVideoCardType(type:"Integrated")
        return builder
    }

    public func makeGamingLaptop(builder:LaptopBuilder)->LaptopBuilder{
        builder.reset()
        builder.setCPU(type:"i7-7700HQ")
        builder.setRAM(size:16)
        builder.setDiskType(type:"SDD")
        builder.setDiskSize(size:1024)
        builder.setVideoCardType(type:"Dedicated")
        return builder
    }

    public func makeRegularPC(builder:PCBuilder)->PCBuilder{
        builder.reset()
        builder.setCPU(type:"i5-7100T")
        builder.setRAM(size:4)
        builder.setDiskType(type:"HDD")
        builder.setDiskSize(size:512)
        builder.setVideoCardType(type:"Integrated")
        return builder

    }

    public func makeGamingPC(builder:PCBuilder)->PCBuilder{
        builder.reset()
        builder.setCPU(type:"i7-8700HK")
        builder.setRAM(size:32)
        builder.setDiskType(type:"SDD")
        builder.setDiskSize(size:2048)
        builder.setVideoCardType(type:"Dedicated")
        return builder

    }
}
