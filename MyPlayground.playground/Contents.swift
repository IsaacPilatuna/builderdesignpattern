import UIKit

let director=Director()

var laptopBuilder=LaptopBuilder()
laptopBuilder=director.makeRegularLaptop(builder:laptopBuilder)
let regularLaptop=laptopBuilder.getResult()
print(regularLaptop.description)

var pcBuilder=PCBuilder()
pcBuilder=director.makeGamingPC(builder:pcBuilder)
let gamingPC=pcBuilder.getResult()
print(gamingPC.description)
